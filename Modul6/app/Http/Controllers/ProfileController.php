<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PostModel as Post;
use Auth;
use Storage;
use DB;

class ProfileController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(){
    $id = Auth::user()->id;
    $data = User::with('posts')->find($id);

    return view('profile', ['data' => $data]);

  }

  public function edit_index(){
    $id = Auth::user()->id;
    $data = User::find($id);

    return view('edit-profile', ['data' => $data]);
  }

  public function edit_proses(Request $request){

    $id = Auth::user()->id;
    $data_user = User::find($id);

    if ($request->has('profile')) {
      Storage::disk('public')->delete($data_user->avatar);
      $path = $request->file('profile')->store('avatars', 'public');
    }else{
      $path = $data_user->avatar;
    }

    User::where('id', $id)->update([
      'title' => $request->title,
      'description' => $request->description,
      'url' => $request->url,
      'avatar' => $path,
    ]);

    return redirect('/profile');

  }

}
