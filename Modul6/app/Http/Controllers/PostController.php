<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\PostsModel as Posts;
use App\Komentar_posts as Komentar_posts;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }


  public function likes(Request $request)
  {
    $id = $request->button_likes;

    $likes = Posts::find($id)->increment('likes');

    return redirect('/home');
  }

  public function komentar(Request $request)
  {
    $komentar = $request->komentar;
    $user_id = Auth::user()->id;
    $post_id = $request->button_komen;

    Komentar_posts::insert([
      'user_id' => $user_id,
      'post_id' => $post_id,
      'comment' => $komentar,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);

    return redirect('/home');
  }

  public function detail($id){
    $data = Posts::with('users')
    ->with('komentar_post')
    ->where('id', $id)
    ->get();

    //return $data[0]->users->name;

    return view('detail', ['post' => $data[0]] );
  }

  public function index(){
    return view('tambah-post');
  }

  public function tambah(Request $request){

    $path = $request->file('image')->store('posts', 'public');
    Posts::insert([
      'user_id' => Auth::user()->id,
      'caption' => $request->caption,
      'image' => $path,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);

    return redirect('/home');

  }

}
