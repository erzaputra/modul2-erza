@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center mx-5">
    <div class="col-12">
      <h1>Edit Profile</h1>
    </div>
    <div class="col-12">
      <form method="post" action="/edit-profile/proses" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" name="title" value="{{ $data->title }}">
        </div>
        <div class="form-group">
          <label>Description</label>
          <input type="text" class="form-control" name="description" value="{{ $data->description }}">
        </div>
        <div class="form-group">
          <label>URL</label>
          <input type="text" class="form-control" name="url" value="{{ $data->url }}">
        </div>
        <div class="form-group">
          <label>Profile Image</label>
          <input type="file" name="profile" class="form-control-file">
        </div>
        <button type="submit" class="btn btn-primary">Edit Profile</button>
      </form>
    </div>
  </div>
</div>
@endsection
