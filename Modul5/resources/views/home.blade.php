@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($posts as $post)
        <div class="col-md-8">
            <div class="card my-3">
                <div class="card-header">
                  <img class="rounded-circle" src="{{ $post->users->avatar }}" alt="foto" height="50px" width="50px">
                  {{ $post->users->name }}
                </div>
                <div class="card-body">
                  <img src="{{ $post->image }}" alt="foto-konten" width="100%">
                </div>
                <div class="card-footer">
                  <b>{{ $post->users->email }}</b>
                  <p>{{ $post->caption }}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
