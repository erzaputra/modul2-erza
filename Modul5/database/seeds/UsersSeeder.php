<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon; //generate time and date

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name' => 'Erza Ganteng',
          'email' => 'erza@gmail.com',
          'password' => bcrypt('erza'),
          'avatar' => 'erza.jpg',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
          'name' => 'Putra Ganteng',
          'email' => 'putra@gmail.com',
          'password' => bcrypt('putra'),
          'avatar' => 'putra.jpg',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now(),
        ]);
    }
}
