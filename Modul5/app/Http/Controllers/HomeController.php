<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\PostsModel as Posts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

      // $data = DB::table('posts')
      // ->join('users', 'user_id', '=', 'users.id')
      // ->get();

      $data = Posts::with('users')->get();

      return view('home', ['posts' => $data] );
    }
}
