<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<link rel="stylesheet" href="style.css">

	<title>Menu</title>
</head>
<body>

	<?php 

	$nama = $_GET['nama'];
	$nomor = $_GET['nomor'];
	$tanggal = $_GET['tanggal'];
	$driver = $_GET['driver'];
	if (isset($_GET['kantongbelanja'])) {
		$kantongbelanja = $_GET['kantongbelanja'];
	}else{
		$kantongbelanja = "Tidak Bawa";
	}
	

	?>

	<div class="container-fluid">
		<div class="col-6 fixed-top menu-data">
			<h1>~Data Driver Ojol~</h1>
			<p><b>Nama</b></p>
			<p><?php echo $nama; ?></p>
			<p><b>Nomor Telepon</b></p>
			<p><?php echo $nomor; ?></p>
			<p><b>Tanggal</b></p>
			<p><?php echo $tanggal; ?></p>
			<p><b>Asal Driver</b></p>
			<p><?php echo $driver; ?></p>
			<p><b>Bawa Kantong</b></p>
			<p><?php echo $kantongbelanja; ?></p>
			<center>
				<button onclick="window.history.back()" type="button" class="btn btn-primary"><< Kembali</button>
			</center>
		</div>
		<div class="row">
			<div class="col-6 menu-data"></div>
			<div class="col-6 menu-data">
				<h1>~Menu~</h1>
				<p>Pilih Menu</p>
				<form action="nota.php" method="post" onsubmit="return confirm('Apakah Anda Yakin?');">
					<table class="table menu-table">
						<tr>
							<td>
								<input class="form-check-input" type="checkbox" name="menu[]" value="28000">
								<label class="form-check-label">
									<b>Es Coklat Susu
									</b>
								</label>
							</td>
							<td>
								Rp. 28.000,-
							</td>
						</tr>
						<tr>
							<td>
								<input class="form-check-input" type="checkbox" name="menu[]" value="18000">
								<label class="form-check-label">
									<b>Es Susu Matcha
									</b>
								</label>
							</td>
							<td>
								Rp. 18.000,-
							</td>
						</tr>
						<tr>
							<td>
								<input class="form-check-input" type="checkbox" name="menu[]" value="15000">
								<label class="form-check-label">
									<b>Es Susu Mojicha
									</b>
								</label>
							</td>
							<td>
								Rp. 15.000,-
							</td>
						</tr>
						<tr>
							<td>
								<input class="form-check-input" type="checkbox" name="menu[]" value="30000">
								<label class="form-check-label">
									<b>Es Matcha Latte
									</b>
								</label>
							</td>
							<td>
								Rp. 30.000,-
							</td>
						</tr>
						<tr>
							<td>
								<input class="form-check-input" type="checkbox" name="menu[]" value="21000">
								<label class="form-check-label">
									<b>Es Taro Susu
									</b>
								</label>
							</td>
							<td>
								Rp. 21.000,-
							</td>
						</tr>
						<tr>
							<td>
								<label class="label">Nomor Order</label>
							</td>
							<td>
								<input type="number" class="form-control" name="nomor_order" required>
							</td>
						</tr>
						<tr>
							<td>
								<label class="label">Nama Pemesan</label>
							</td>
							<td>
								<input type="text" class="form-control" name="nama_pemesan" required>
							</td>
						</tr>
						<tr>
							<td>
								<label class="label">Email</label>
							</td>
							<td>
								<input type="email" class="form-control" name="email" required>
							</td>
						</tr>
						<tr>
							<td>
								<label class="label">Alamat Order</label>
							</td>
							<td>
								<input type="text" class="form-control" name="alamat_order" required>
							</td>
						</tr>
						<tr>
							<td>
								<label class="label">Member</label>
							</td>
							<td>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="member" value="Ya" required>
									<label class="form-check-label" for="inlineRadio1">Ya</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="member" value="Tidak" required>
									<label class="form-check-label" for="inlineRadio1">Tidak</label>
								</div>
							</td>
						</tr>
						<tr>
							<td>
								<label class="label">Metode Pembayaran</label>
							</td>
							<td>
								<select class="custom-select" name="method" required>
									<option value="">Pilih Metode Pembayaran</option>
									<option value="Cash">Cash</option>
									<option value="E-Money (OVO/Gopay)">E-Money (OVO/Gopay)</option>
									<option value="Credit Card">Credit Card</option>
									<option value="Lainnya">Lainnya</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="col-sm-12" >
								<button type="submit" class="btn btn-primary" style="min-width: 100%" name="cetak">CETAK STRUK</button>
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>