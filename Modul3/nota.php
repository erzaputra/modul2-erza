<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<link rel="stylesheet" href="style.css">

	<title>Form</title>
</head>
<body>

	<?php 

	if (isset($_POST['menu'])) {
		$menu['list'] = $_POST['menu'];
	}else{
		$menu['list'][0] = 0;
	}

	$total = 0;
	for ($i=0; $i < count($menu['list']); $i++) { 
		$total = $total + $menu['list'][$i];
	}
	
	$nomor_order = $_POST['nomor_order'];
	$nama = $_POST['nama_pemesan'];
	$email = $_POST['email'];
	$alamat = $_POST['alamat_order'];

	$member = $_POST['member'];
	if ($member == "Ya") {
		$total = $total - ($total * (10/100));
	}
	$total = number_format($total, 2,",",".");

	$method = $_POST['method'];

	?>

	<div class="container-fluid">
		<div class="row">
			<div class="col-12 container-data">
				<h1>Transaksi Pemesanan</h1>
				<p>Terima kasih telah berbelanja Kopi Susu Duarrr!</p>
				
				<table class="table nota-table">
					<tr>
						<td colspan="2">
							<h1 style="border-top: none !important; text-align: left; margin: 30px 0px;">Rp. <?php echo $total ?>,-</h1>
						</td>
					</tr>
					<tr>
						<td><b>ID</b></td>
						<td><?php echo $nomor_order; ?></td>
					</tr>
					<tr>
						<td><b>Nama</b></td>
						<td><?php echo $nama; ?></td>
					</tr>
					<tr>
						<td><b>Email</b></td>
						<td><?php echo $email; ?></td>
					</tr>
					<tr>
						<td><b>Alamat</b></td>
						<td><?php echo $alamat; ?></td>
					</tr>
					<tr>
						<td><b>Member</b></td>
						<td><?php echo $member; ?></td>
					</tr>
					<tr>
						<td><b>Pembayaran</b></td>
						<td><?php echo $method; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>
</html>