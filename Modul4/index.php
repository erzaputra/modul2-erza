<?php

include "connection.php";

?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="image/EAD.png">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="asset/style.css">

  <title>Online Course Shop</title>
</head>
<body>

  <?php include "partition/navbar.php"; ?>

  <div class="container-fluid">
    <div class="header">
      <h1>Hello Coders</h1>
      <p>Welcome to our store, please take a look for the products you might buy</p>
    </div>
    <div class="row course">
      <div class="col-sm-4">
        <div class="card">
          <img src="image/web.jpg" class="card-img-top" alt="web">
          <div class="card-body">
            <h5 class="card-title"><b>Learning Basic Web Programming</b></h5>
            <h5>Rp 210.000,-</h5>
            <p class="card-text">Went to be able to make a website ? Learn basic components such as HTML, CSS and Javascript in this class curricullum</p>
          </div>
          <div class="card-footer">
            <form class="" action="process/buy.php" method="post">
              <button type="submit" name="web" class="btn btn-primary w-100">Buy</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <img src="image/java.jpg" class="card-img-top" alt="java">
          <div class="card-body">
            <h5 class="card-title"><b>Starting Programming in Java</b></h5>
            <h5>Rp 150.000,-</h5>
            <p class="card-text">Learn Java Language for you who want to learn the most popular Object-Oriented Programming (PBO) concepts for developing application</p>
          </div>
          <div class="card-footer">
            <form class="" action="process/buy.php" method="post">
              <button type="submit" name="java" class="btn btn-primary w-100">Buy</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <img src="image/phyton.jpg" class="card-img-top" alt="phyton">
          <div class="card-body">
            <h5 class="card-title"><b>Starting Programming in Phyton</b></h5>
            <h5>Rp 200.000,-</h5>
            <p class="card-text">Learn Phyton - Fundamental various current industry trends Data Science, Machine Learning, Infrastructure Management</p>
          </div>
          <div class="card-footer">
            <form class="" action="process/buy.php" method="post">
              <button type="submit" name="phyton" class="btn btn-primary w-100">Buy</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
          <img src="image/phyton.jpg" class="card-img-top" alt="phyton">
          <div class="card-body">
            <h5 class="card-title"><b>Starting Programming in PHP</b></h5>
            <h5>Rp 500.000,-</h5>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
          </div>
          <div class="card-footer">
            <form class="" action="process/buy.php" method="post">
              <button type="submit" name="php" class="btn btn-primary w-100">Buy</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php include "partition/footer.php"; ?>

    <form class="" action="process/login.php" method="post">
      <div class="modal fade" id="login-modal" tabhome="-1">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Login</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label for="">Email Address</label>
                <input class="form-control" type="email" name="email" >
              </div>
              <div class="form-group">
                <label for="">Password</label>
                <input class="form-control" type="password" name="password" >
              </div>
            </div>
            <div class="modal-footer text-right">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" name="login">Login</button>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form class="" action="process/daftar.php" method="post">
      <div class="modal fade" id="daftar-modal" tabhome="-1">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Daftar</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label >Email Address</label>
                <input class="form-control" type="email" name="email" >
              </div>
              <div class="form-group">
                <label >Username</label>
                <input class="form-control" type="text" name="username" >
              </div>
              <div class="form-group">
                <label >Password</label>
                <input class="form-control" type="password" name="password" >
              </div>
              <div class="form-group">
                <label >Confirm Password</label>
                <input class="form-control" type="password" name="repassword" >
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" name="daftar">Daftar</button>
            </div>
          </div>
        </div>
      </div>
    </form>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
