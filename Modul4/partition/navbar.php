<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <ul class="navbar-nav mr-auto">
    <a class="navbar-brand" href="index.php">
      <img src="image/EAD.png" width="145" height="50" alt="logo ead">
    </a>
  </ul>
  <ul class="navbar-nav">
    <?php if (isset($_SESSION['id'])) { ?>
      <li class="nav-item">
        <a class="nav-link" href="cart.php" >
          <span class="fa fa-shopping-cart"></span>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <?php echo $_SESSION['username']; ?>
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="profil.php">Profile</a>
          <a class="dropdown-item" href="process/logout.php">Logout</a>
        </div>
      </li>
    <?php  } else { ?>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#login-modal">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#daftar-modal">Register</a>
      </li>
    <?php } ?>
  </ul>
</nav>
