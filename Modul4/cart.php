<?php

include "connection.php";

?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="image/EAD.png">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="asset/style.css">

  <title>Cart</title>
</head>
<body>
  <?php include "partition/navbar.php" ?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="shadow-sm table-cart">
          <table class="" style="width:100%">
            <tr>
              <th>No</th>
              <th>Product</th>
              <th>Price</th>
              <th></th>
            </tr>
            <?php

            $id = $_SESSION['id'];
            $hasil = mysqli_query($conn, "SELECT * FROM cart_table WHERE user_id = '$id'");
            $iterasi = 1;
            $total = 0;
            while($baris = mysqli_fetch_assoc($hasil)) {
              $total = $total + $baris['price'];

              ?>
              <tr>
                <td><?php echo $iterasi++; ?></td>
                <td><?php echo $baris['product']; ?></td>
                <td><?php echo $baris['price']; ?></td>
                <td>
                  <form class="" action="process/delete.php" method="post">
                    <button type="submit" name="delete" class="btn btn-danger" value="<?php echo $baris['id']; ?>">x</button>
                  </form>
                </td>
              </tr>
            <?php } ?>
            <tr>
              <td colspan="4">
                <hr>
              </td>
            </tr>
            <tr>
              <td colspan="2">Total</td>
              <td><?php echo $total; ?></td>
            </tr>
          </table>
        </div>
      </div>
    </div>

    <?php include "partition/footer.php" ?>

  </div>


  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
