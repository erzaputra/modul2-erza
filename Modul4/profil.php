<?php

include "connection.php";

if(!isset($_SESSION['id'])){
  echo "<script>alert('anda harus login dahulu!');
  window.location.href = 'index.php';
  </script>";
}else{
  $id = $_SESSION['id'];
}

$id = $_SESSION['id'];
$hasil = mysqli_query($conn, "SELECT * FROM users_table WHERE id = '$id'");
$hasil = mysqli_fetch_assoc($hasil);

?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="icon" href="image/EAD.png">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="asset/style.css">

  <title>Profile</title>
</head>
<body>
  <?php include "partition/navbar.php" ?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="form-profile shadow-sm" >
          <form method="post" action="process/save.php">
            <center>
              <h2>Profile</h2>
            </center>

            <table width="100%" cellpadding="7">
              <tr>
                <td width="20%">Email</td>
                <td><?php echo $hasil["email"]; ?></td>
              </tr>
              <tr>
                <td>Username</td>
                <td>
                  <input class="form-control" type="text" name="username" value="<?php echo $hasil["username"]; ?>">
                </td>
              </tr>
              <tr>
                <td>Mobile Number</td>
                <td><input class="form-control" type="number" name="mobile_number" value="<?php echo $hasil["mobile_number"]; ?>"></td>
              </tr>
              <tr>
                <td colspan="2">
                  <hr>
                </td>
              </tr>
              <tr>
                <td>New Password</td>
                <td><input class="form-control" type="password" name="password"></td>
              </tr>
              <tr>
                <td>Confirm Password</td>
                <td><input class="form-control" type="password" name="repassword"></td>
              </tr>
              <tr>
                <td colspan="2">
                  <div class="form-group">
                    <button type="submit" name="save" class="btn btn-primary w-100">Save</button>
                  </div>
                  <div class="form-group">
                    <button type="button" name="cancel" class="btn w-100">Cancel</button>
                  </div>
                </td>
              </tr>
            </table>
          </form>
        </div>
      </div>
    </div>
    <?php include "partition/footer.php" ?>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
